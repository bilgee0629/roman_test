'use strict';

var numbers = {
    'M': 1000,
    'CM': 900,
    'D': 500,
    'CD': 400,
    'C': 100,
    'XC': 90,
    'L': 50,
    'XL': 40,
    'X': 10,
    'IX': 9,
    'V': 5,
    'IV': 4,
    'I': 1
};

module.exports.convertToRomanNumeral = function(number) {

    if (number === undefined || number === null || number.length === 0) { throw new Error('value required'); }
    if (!Number.isInteger(number)) { throw new Error("invalid value"); }
    if (number > 3999 || number < 1) { throw new Error("invalid range"); }

    var result = '';
    
    for (var key in numbers) {

        while (number % numbers[key] < number) {
            result += key;
            number -= numbers[key];
        }

    }

    return result;
};

module.exports.convertFromRomanNumeral = function(number) {

    if (number === undefined || number === null || number.length === 0) { throw new Error('value required'); }

    var result = 0;

    for (var i = number.length - 1; i >= 0; i--) {

        if( numbers[number[i]] === undefined) {
            throw new Error('invalid value');
        }

        if (numbers[number[i]] > numbers[number[i - 1]]) {
            result = result + numbers[number[i]] - numbers[number[i - 1]];
            i--;
        } else {
            result += numbers[number[i]];
        }

    }

    return result;
};

