'use strict';

var expect = require('chai').expect;
var roman = require('./index.js');

describe('#roman', function() {

    it('null', function() {

        try {
            var result = roman.convertFromRomanNumeral(null);
        } catch(err) {
            console.error('error', err.message);
            expect(err.message).to.equal('value required');
        }

    });

    it('empty value', function() {

        try {
            var result = roman.convertFromRomanNumeral('');
        } catch(err) {
            console.error('error', err.message);
            expect(err.message).to.equal('value required');
        }

    });

    it('invalid-value', function() {

        try {
            var result = roman.convertFromRomanNumeral('invalid-value');
        } catch(err) {
            console.error('error', err.message);
            expect(err.message).to.equal('invalid value');
        }

    });

    it('DCCVII', function() {

        try {
            var result = roman.convertFromRomanNumeral('DCCVII');
            expect(result).to.equal(707);
        } catch(err) {
            console.error('error', err.message);
            expect(err.message).to.equal('invalid value');
        }
        
    });

    it('0', function() {

        try {
            var result = roman.convertToRomanNumeral(0);
        } catch(err) {
            console.error('error', err.message);
            expect(err.message).to.equal('invalid range');
        }

    });

    it('1', function() {

        try {
            var result = roman.convertToRomanNumeral(1);
            expect(result).to.equal('I');
        } catch(err) {
            console.error('error', err.message);
        }

    });

    it('3', function() {

        try {
            var result = roman.convertToRomanNumeral(3);
            expect(result).to.equal('III');
        } catch(err) {
            console.error('error', err.message);
        }

    });

    it('4', function() {

        try {
            var result = roman.convertToRomanNumeral(4);
            expect(result).to.equal('IV');
        } catch(err) {
            console.error('error', err.message);
        }

    });

    it('5', function() {

        try {
            var result = roman.convertToRomanNumeral(5);
            expect(result).to.equal('V');
        } catch(err) {
            console.error('error', err.message);
        }

    });

    it('I', function() {

        try {
            var result = roman.convertFromRomanNumeral('I');
            expect(result).to.equal(1);
        } catch(err) {
            console.error('error', err.message);
        }
        
    });

    it('III', function() {

        try {
            var result = roman.convertFromRomanNumeral('III');
            expect(result).to.equal(3);
        } catch(err) {
            console.error('error', err.message);
        }
        
    });

    it('IV', function() {

        try {
            var result = roman.convertFromRomanNumeral('IV');
            expect(result).to.equal(4);
        } catch(err) {
            console.error('error', err.message);
        }
        
    });

    it('V', function() {

        try {
            var result = roman.convertFromRomanNumeral('V');
            expect(result).to.equal(5);
        } catch(err) {
            console.error('error', err.message);
        }
        
    });

    it('1968', function() {

        try {
            var result = roman.convertToRomanNumeral(1968);
            expect(result).to.equal('MCMLXVIII');
        } catch(err) {
            console.error('error', err.message);
        }
        
    });

    it('2999', function() {

        try {
            var result = roman.convertToRomanNumeral(2999);
            expect(result).to.equal('MMCMXCIX');
        } catch(err) {
            console.error('error', err.message);
        }
        
    });

    it('10000', function() {

        try {
            var result = roman.convertToRomanNumeral(10000);
        } catch(err) {
            console.error('error', err.message);
            expect(err.message).to.equal('invalid range');
        }
        
    });

    it('3000', function() {

        try {
            var result = roman.convertToRomanNumeral(3000);
            expect(result).to.equal('MMM');
        } catch(err) {
            console.error('error', err.message);
        }
        
    });

    it('CDXXIX', function() {

        try {
            var result = roman.convertFromRomanNumeral('CDXXIX');
            expect(result).to.equal(429);
        } catch(err) {
            console.error('error', err.message);
        }
        
    });

    it('MCDLXXXII', function() {

        try {
            var result = roman.convertFromRomanNumeral('MCDLXXXII');
            expect(result).to.equal(1482);
        } catch(err) {
            console.error('error', err.message);
        }
        
    });

    it('MCMLXXX', function() {

        try {
            var result = roman.convertFromRomanNumeral('MCMLXXX');
            expect(result).to.equal(1980);
        } catch(err) {
            console.error('error', err.message);
        }
        
    });

    it('MMMMDMXCIX', function() {

        try {
            var result = roman.convertFromRomanNumeral('MMMMDMXCIX');
            expect(result).to.equal(1);
        } catch(err) {
            console.error('error', err.message);
        }
        
    });

    it('MMMMCMXCIX', function() {

        try {
            var result = roman.convertFromRomanNumeral('MMMMCMXCIX');
            expect(result).to.equal(4999);
        } catch(err) {
            console.error('error', err.message);
        }
        
    });

});


